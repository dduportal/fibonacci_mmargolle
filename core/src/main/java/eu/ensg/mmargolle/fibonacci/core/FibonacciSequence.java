package eu.ensg.mmargolle.fibonacci.core;

import java.util.HashMap;
import java.math.BigInteger;

/**
 * Computes elements of the Fibonacci Sequence
 */
public class FibonacciSequence {

    /**
     * Cache for retrieving already calculated values
     */
    private static HashMap<Long, BigInteger> cache = new HashMap<>();

    /**
     * Computes an element of the Fibonacci Sequence given its rank
     * @param rank The rank of the element to compute
     * @return The value for the given rank
     */
    public static BigInteger get(long rank) {
        if (rank <= 0) return BigInteger.ZERO;
        if (rank == 1) return BigInteger.ONE;
        if (cache.containsKey(rank)) return cache.get(rank);
        BigInteger value = get(rank - 1).add(get(rank - 2));
        cache.put(rank, value);
        return value;
    }
}
