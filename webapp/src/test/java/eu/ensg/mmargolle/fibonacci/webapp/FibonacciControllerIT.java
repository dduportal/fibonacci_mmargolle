package eu.ensg.mmargolle.fibonacci.webapp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FibonacciControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private FibonacciController fibonacciController;

    @Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.fibonacciController).build();// Standalone context
    }

    @Test
    public void rank13ShouldReturn233() throws Exception {
        this.mockMvc.perform(get("/fibonacci/{rank}", 13))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("233"));
    }

}