package eu.ensg.mmargolle.maven.plugins.fibonacci;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.codehaus.plexus.PlexusTestCase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

public class FibonacciMojoIT extends AbstractMojoTestCase {

    public void testPluginExists() throws Exception {
        try {
            FibonacciMojo mojo = getMojo();
            assertNotNull(mojo);
        } catch(Exception ex) {
            fail();
            throw ex;
        }
    }

    public void testInConsoleOutput() throws Exception {

        FibonacciMojo mojo = getMojo();

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        PrintStream testConsole = new PrintStream(outStream);
        PrintStream trueConsole = System.out;

        System.setOut(testConsole);
        mojo.execute(); // Called with parameter rank = 13 in test config pom.xml
        System.setOut(trueConsole);

        assertEquals("233", outStream.toString().trim());
    }

    private FibonacciMojo getMojo() throws Exception {
        File pluginConfig = new File( PlexusTestCase.getBasedir(), "src/test/resources/pom.xml" );
        return( (FibonacciMojo) super.lookupMojo("fibonacci", pluginConfig) );
    }
}