package eu.ensg.mmargolle.maven.plugins.fibonacci;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import eu.ensg.mmargolle.fibonacci.core.FibonacciSequence;

/**
 * Maven plugin for computing elements of the Fibonacci Sequence
 */
@Mojo(name="fibonacci")
public class FibonacciMojo extends AbstractMojo {

    /**
     * The rank of the element to compute
     */
    @Parameter(property="rank", required=true)
    private long rank;

    /**
     * Prints the value computed for the given rank in the Fibonacci Sequence
     */
    public void execute() {
        System.out.println(String.valueOf(FibonacciSequence.get(rank)));
    }

}
